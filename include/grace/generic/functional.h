#pragma once

#include "grace/config.h"

#include <functional>

/* This file provides device-compatible implementations of most of the standard
 * library header <functional>. This allows generic host- and device-compatible
 * code to be written that will compile (for the host) when CUDA/Thrust is not
 * available.
 */

/* This file used to used std::function_unary and std::function_binary where were
 * deprecated.  I (Eric Tittley) replaced them with std_function which worked, in
 * that the compiler stopped complaining about deprecated features. But I can't 
 * help but think there's a more elegant solution. */

namespace grace {

//
// Arithmetic
//

template <typename T>
struct plus : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs + rhs;
    }
};

template <typename T>
struct minus : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs - rhs;
    }
};

template <typename T>
struct multiplies : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs * rhs;
    }
};

template <typename T>
struct divides : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs / rhs;
    }
};

template <typename T>
struct modulus : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs % rhs;
    }
};


template <typename T>
struct maximum : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs < rhs ? rhs : lhs;
    }
};

template <typename T>
struct minimum : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs < rhs ? lhs : rhs;
    }
};

template <typename T>
struct negate : std::function<T(T)>
{
    GRACE_HOST_DEVICE T operator()(const T& x) const
    {
        return -x;
    }
};


//
// Comparisons
//

template <typename T>
struct equal_to : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs == rhs;
    }
};

template <typename T>
struct not_equal_to : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs != rhs;
    }
};

template <typename T>
struct greater : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs > rhs;
    }
};

template <typename T>
struct less : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs < rhs;
    }
};

template <typename T>
struct greater_equal : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs >= rhs;
    }
};

template <typename T>
struct less_equal : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs <= rhs;
    }
};


//
// Logical operations
//

template <typename T>
struct logical_and : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs && rhs;
    }
};

template <typename T>
struct logical_or : std::function<bool(T,T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& lhs, const T& rhs) const
    {
        return lhs || rhs;
    }
};

template <typename T>
struct logical_not : std::function<bool(T)>
{
    GRACE_HOST_DEVICE bool operator()(const T& x) const
    {
        return !x;
    }
};


//
// Bitwise operations
//

template <typename T>
struct bit_and : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs & rhs;
    }
};

template <typename T>
struct bit_or : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs | rhs;
    }
};

template <typename T>
struct bit_xor : std::function<T(T, T)>
{
    GRACE_HOST_DEVICE T operator()(const T& lhs, const T& rhs) const
    {
        return lhs ^ rhs;
    }
};

template <typename T>
struct bit_not : std::function<T(T)>
{
    GRACE_HOST_DEVICE T operator()(const T& x) const
    {
        return ~x;
    }
};


//
// Negators
//

template<typename Predicate>
struct unary_negate : std::function<bool(typename Predicate::argument_type)>
{
    GRACE_HOST_DEVICE explicit unary_negate(const Predicate& pred)
        : pred(pred) {}

    GRACE_HOST_DEVICE
    bool operator()(const typename Predicate::argument_type& x) const
    {
        return !pred(x);
    }

protected:
    Predicate pred;
};

template<typename Predicate>
struct binary_negate
    : std::function<bool(typename Predicate::first_argument_type,
                         typename Predicate::second_argument_type)>
{
    GRACE_HOST_DEVICE explicit binary_negate(const Predicate& pred)
        : pred(pred) {}

    GRACE_HOST_DEVICE
    bool operator()(const typename Predicate::first_argument_type& x,
                    const typename Predicate::second_argument_type& y) const
    {
        return !pred(x, y);
    }

protected:
    Predicate pred;
};

} // namespace grace
